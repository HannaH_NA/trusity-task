import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BaseComponent } from './layouts/base/base.component'
import { EmployeeComponent } from './pages/employee/employee.component';
import { AddEmployeeComponent } from './pages/employee/add-employee/add-employee.component';
import { EditEmployeeComponent } from './pages/employee/edit-employee/edit-employee.component';
import { ViewEmployeeComponent } from './pages/employee/view-employee/view-employee.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'employees', component: EmployeeComponent },
      { path: 'employee-add', component: AddEmployeeComponent },
      { path: 'employee-edit/:id', component: EditEmployeeComponent },
      { path: 'employee-view/:id', component: ViewEmployeeComponent },



    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
