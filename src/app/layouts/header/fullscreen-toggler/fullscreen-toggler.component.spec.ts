import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullscreenTogglerComponent } from './fullscreen-toggler.component';

describe('FullscreenTogglerComponent', () => {
  let component: FullscreenTogglerComponent;
  let fixture: ComponentFixture<FullscreenTogglerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullscreenTogglerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullscreenTogglerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
